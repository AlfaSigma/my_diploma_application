﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Interface
{
	/// <summary>
	/// Логика взаимодействия для AddPerson.xaml
	/// </summary>
	public partial class AddPerson : Window
	{
		public AddPerson()
		{
			this.InitializeComponent();
			
			// Вставьте ниже код, необходимый для создания объекта.
		}

		private void close_addPerson(object sender, System.Windows.RoutedEventArgs e)
		{
			this.Close();// TODO: Добавьте здесь реализацию обработчика событий.
		}


		private void window_move(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			this.DragMove();
		}
	}
}